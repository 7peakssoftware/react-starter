const path = require('path');
const webpack = require('webpack');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const AsyncChunkNames = require('webpack-async-chunk-names-plugin');
const BrotliPlugin = require('brotli-webpack-plugin');
const LoadablePlugin = require('@loadable/webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WebpackPwaManifest = require('webpack-pwa-manifest');
const { InjectManifest } = require('workbox-webpack-plugin');

const rootDir = path.resolve(__dirname, '..');
const buildPath = path.join(rootDir, 'build');
const appPath = path.resolve(rootDir, 'src');
const clientEntry = path.resolve(appPath, 'client', 'index.jsx');

const isProduction = process.env.NODE_ENV === 'production';

// eslint-disable-next-line no-console
console.info('NODE_ENV:', process.env.NODE_ENV);

const rules = [
  {
    test: /\.(js|jsx)$/,
    use: [
      {
        loader: 'babel-loader'
      }
    ],
    exclude: /node_modules/
  },
  {
    test: /\.(svg|png|jpg|jpeg|gif|woff)$/i,
    use: 'url-loader?limit=10000'
  },
  {
    test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
    loader: 'url-loader?limit=10000&mimetype=application/font-woff'
  },
  {
    test: /\.css$/,
    use: ['css-loader'] /* required for loading typeface-roboto */
  }
];

let plugins = [
  new CleanWebpackPlugin(),
  new LoadablePlugin({ filename: 'client-stats.json' }),
  new webpack.DefinePlugin({
    ENV_NAME: JSON.stringify(!isProduction ? process.env.NODE_ENV.toLocaleUpperCase() : '')
  }),
  new AsyncChunkNames() /* remember async import name */
];

if (!isProduction) {
  plugins.push(new webpack.HotModuleReplacementPlugin());
} else {
  plugins = [
    ...plugins,
    new HtmlWebpackPlugin({
      filename: 'cache-index.html',
      title: '7 Peaks - React Starter',
      inject: 'body',
      meta: {
        'X-UA-Compatible': {
          'http-equiv': 'X-UA-Compatible',
          content: 'IE=edge'
        },
        viewport: 'width=device-width, initial-scale=1, shrink-to-fit=no',
        charset: 'UTF-8'
      },
      minify: true
    }),
    new WebpackPwaManifest({
      filename: 'manifest.json',
      name: '7 Peaks - React Starter',
      orientation: 'portrait',
      display: 'standalone',
      start_url: '.',
      inject: true,
      fingerprints: true,
      ios: true,
      theme_color: 'rgb(115, 192, 124)',
      background_color: '#8291a4',
      icons: []
    }),
    new InjectManifest({
      swSrc: path.join(appPath, 'service-worker.js'),
      swDest: path.join(buildPath, 'sw.js'),
      exclude: [/\.br$/, /\.gz$/]
    }),
    new CompressionPlugin({
      algorithm: 'gzip'
    }),
    new BrotliPlugin()
  ];
}

const resolve = {
  modules: ['node_modules', appPath],
  extensions: ['.jsx', '.js'],
  enforceExtension: false
};

const clientEntries = ['babel-polyfill', clientEntry];

if (!isProduction) {
  clientEntries.unshift('webpack-hot-middleware/client?reload=true');
}

module.exports = {
  name: 'client',
  mode: process.env.NODE_ENV,
  devtool: 'cheap-eval-source-map',
  entry: {
    client: clientEntries
  },
  output: {
    filename: 'bundle.[name].js',
    chunkFilename: 'bundle.[name].js',
    path: buildPath,
    publicPath: '/'
  },
  module: {
    rules
  },
  devServer: {
    logLevel: 'silent',
    serverSideRender: true
  },
  resolve,
  plugins,
  optimization: {
    splitChunks: {
      cacheGroups: {
        default: false /* turn off default setting */,
        vendors: false /* turn off default setting */,
        vendor: {
          chunks: 'initial', // AKA sync module
          /* import file path containing node_modules */
          name: 'vendor',
          /* always output chunk */
          enforce: true
        }
      }
    },
    minimizer: isProduction
      ? [
        new UglifyJSPlugin({
          uglifyOptions: {
            compress: {
              hoist_funs: true
            },
            output: {
              comments: false
            }
          }
        })
      ]
      : undefined
  }
};
