const path = require('path');
const nodeExternals = require('webpack-node-externals');
const webpack = require('webpack');

/*
 *  Path Resolver
 */
const rootDir = path.resolve(__dirname, '..');
const buildPath = path.join(rootDir, 'build');
const appPath = path.resolve(rootDir, 'src');
const serverEntry = path.resolve(appPath, 'server', 'app.jsx');

const isProduction = process.env.NODE_ENV === 'production';

const plugins = [
  new webpack.DefinePlugin({
    ENV_NAME: JSON.stringify(`${!isProduction ? process.env.NODE_ENV.toLocaleUpperCase() : ''}`)
  }),
  new webpack.optimize.LimitChunkCountPlugin({
    maxChunks: 1 /* not necessary to split chunk for server code */
  }),
  new webpack.NamedModulesPlugin()
];

const resolve = {
  modules: ['node_modules', appPath],
  extensions: ['.jsx', '.js'],
  enforceExtension: false
};

module.exports = {
  name: 'server',
  mode: process.env.NODE_ENV,
  target: 'node',
  externals: ['@loadable/component', nodeExternals()],
  entry: serverEntry,
  output: {
    filename: 'bundle.server.js',
    path: buildPath,
    libraryTarget: 'commonjs2', // required by hot-server-middleware
    publicPath: '/'
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: [
          {
            loader: 'babel-loader'
          }
        ],
        exclude: /node_modules/
      },
      {
        test: /\.(svg|png|jpg|jpeg|gif|woff)$/i,
        use: 'url-loader?limit=10000'
      }
    ]
  },
  resolve,
  plugins,
  optimization: {
    minimize: false /* don't need to minize server code */
  }
};
