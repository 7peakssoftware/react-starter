# 7 Peaks Software - React Starter

A scaffolding project that contains a certain set of standard tools for a react app.

# What’s this project offer?

This starter project includes:

- React, JSX, ES6 support.
- A live development server with server-side rendering.
- A build script to bundle JS, CSS, and images for production.
- Code linting and formatting.
- Code splitting

# Table of Contents

- [How to create a new app](#how-to-create-a-new-app)
  - [Update a remote git repository](#update-a-remote-git-repository)
- [Project Structure](#project-structure)
- [Available Scripts](#available-scripts)
- [Storybook](#storybook)
- [Running Tests](#running-tests)
- [Styled Components](#styled-components)

![screenshot](https://bitbucket.org/repo/A6X4Eng/images/1109538247-sc.png)

# How to create a new app

You will need to have Node 10.16.0 or later on your local development machine, the latest LTS version is preferred. Node version management, nvm (macOS/Linux) or nvm-windows, is recommended to easily switch Node versions between different projects. `Git` client will also be required on your local development machine as well.

Then use the following steps to setup a new app:

1. Clone the project to your local development machine.
   ```bash
   $ git clone https://max_7peaks@bitbucket.org/7peakssoftware/react-starter.gitgit
   ```
2. Update project name and description in `package.json`
3. Run `npm install` to install all node dependencies.
4. Start a development server by using `npm run dev`

## Update a remote git repository

Once you have clone the scaffolding project into your local machine, the remote repository will be pointing to `https://max_7peaks@bitbucket.org/7peakssoftware/react-starter.git`. To push it to new repository when you creating new project, the remote repository URL need to be updated by using the following command

```bash
$ git remote set-url origin <path_to_new_repo>
```

# Project Structure

```
├── .storybook                  # Storybook configuration files
├── config                      # Webpack configuration files
├── src
│   ├── __test__                # Test related files`
│   ├── assets
│   ├── client
│   ├── server
│   ├── shared
|   |    ├── components
|   |    ├── constants
|   |    ├── hooks
|   |    ├── pages
|   |    ├── stores
|   |    ├── utils
├── stories                     # Storybook's stories
├── .bablerc                    # Babel configuration file
├── .editorconfig               # Basic editor configuration file
├── .eslintignore               # ESLint ignore file
├── .eslint                     # ESLint configuration file
├── .nvmrc                      # Node version configuration file
├── .prettierrc                 # Prettier configuration file
├── .eslintrc                   # ESLint configuration file
├── package.json                # NPM configuration file
└── README.md
```

# Available Scripts

| Command      | Description                                         |
| ------------ | --------------------------------------------------- |
| test         | Running all test underneath `__test__`              |
| storybook    | Start storybook server                              |
| lint         | Start linting for the whole project                 |
| lint:fix     | Quick code formatting/linting for the whole project |
| start        | Start Production server                             |
| dev          | Start Development server                            |
| build        | Build bundle for both `client` and `server`         |
| build:client | Build client bundle                                 |
| build:server | Build server bundle                                 |

# Code Linting and Formatting

This project equipped with ESLint and Prettier. The renowned `Airbnb JavaScript Style Guide` was used as a foundation of linting rules. To enable/disable more rules, please update `.eslintrc` on the project root-level. Also editor plug-ins maybe require to enable auto fixing/formatting on file save.

For `Visual Studio Code` please refer to: [ESLint extension](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)

Code linting will be run every time when pushing your code into a remote repository. To skip, please include `--no-verify` tag.

To run lint manually use

```bash
$ npm run lint
```

and, To fix all formatting errors across the whole project run

```bash
$ npm run lint:fix
```

# Storybook

Storybook is a development environment for UI components. It allows an UI components to be developed without interfering with the main app code. It could be considered as an isolated playground environment for you o design and test your components to be sure that they could be reused easily before integrating with the main app.

To create a new UI component, two parts of code are required; one is a component and another one is a story.

You can basically create a new component under `src/components` then import a recently created component into a new stories file which need to be created under `stories` folder. The story file name need to have `.stories` in order to allows `storybook` to pick up.

Please refer to [Storybook official doc](https://storybook.js.org/docs/basics/writing-stories/) to see how to `Writing Stories`.

The command `npm run storybook` can be run to start up Storybook server which will provide a components catalogue and live update when component code get changed.

Two basic add-ons were included with this project which are

1. Knob - allow you to edit React props dynamically using the Storybook UI
2. Viewport - allows your stories to be displayed in different sizes and layouts

you can add more if needed then update a configuration file in `.storybook`

# Running Tests

Jest is a Javascript Testing framework that is broadly used across JS community. This project also uses Jest as its test runner. Jest configurations can be found in `jest.config.json`, additional asset-transform configurations can be found in `config/jest`.

In this project `Jest` was set to look for a test file that has `*.test.*` pattern in `__test__`. Before the test begins, code in `__test__/jest-init.js` will be executed first. Hence, any extra steps that are require before the actual test run, for example; env variable setting, mocking functions, or API intercepting, can be placed there.

To learn more about testing with `Jest` please visit [https://jestjs.io/](https://jestjs.io/)

# Code Splitting

Instead of downloading the entire app before users can use it, code splitting allows the code to be split into small chunks abd beloaded in the fly when needed.

This project uses [`loadable-component`](https://github.com/smooth-code/loadable-components) to support serverside-rendering code splitting via dynamic import().

Here is an example:

### `component/ModuleA/ModuleA.js`

```javascript
const moduleA = () => 'Hello';

export defalut A
```

### `component/ModuleA/index.js`

```javascript
import loadable from '@loadable/component';

export const AsyncModuleA = loadable(() => import('./ModuleA'));
```

### `App.js`

```javascript
import React, { Component } from 'react';
import AsyncModuleA from 'shared/component/ModuleA';

const App = () => (
  <div>
    <AsyncModuleA />
  </div>
);

export default App;
```

This will make ModuleA.js and all its unique dependencies as a separate chunk that only loads when needed.

_**Caveat**:_ Name-export in a component file will causing error when using SSR.

# Styled Components

Style Component is a CSS-in-JS libraly that allows an actaul CSS code to be written to style a component directly without classname mapping.

As a convention, styled-component file have to has `_.style_ suffix.

For example:

### `component/ModuleA/ModuleA.style.js`

```javascript
import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
`;
```

### `component/ModuleA/ModuleA.jsx`

```javascript
import React from 'react';
import { Container } from './ModuleA.style';

export const ModuleA = () => <Container>Her is a content</Container>;
```

styled-component will turn `<Container>` into a `div` element with classname attched when bundling.

## Gobal Style

Sometimes, you want to apply some style for the whole app, `createGlobalStyle` can be used for this case. Navigate to `shared/app.jsx`, you will find `GlobalStyle` const. Any app-wide can be added here.

```javascript
const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    color: blue;
    font-size: 16px;
    font-family: Roboto, Arial, sans-serif;
  }
`;
```

_**Caveat**:_ Styles that are being applied throught a tag selector may not display correctly every time with hot-reloading enabled since the selector won't get hashed. Try to perform full-page refresh to see the latest result.
