import * as React from 'react';
import { hydrate as reactDOMHydrate } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { App } from 'shared/app';
import { loadableReady } from '@loadable/component';
import './register-sw';
import 'typeface-roboto';

let entry = document.getElementById('entry');

if (!entry) {
  entry = document.createElement('div');
  entry.id = 'entry';

  document.body.appendChild(entry);
}

loadableReady(() => {
  const render = (Component) => {
    reactDOMHydrate(
      <React.StrictMode>
        <BrowserRouter>
          <Component />
        </BrowserRouter>
      </React.StrictMode>,
      entry
    );
  };

  render(App);

  if (module.hot) {
    module.hot.accept('shared/app', () => {
      // eslint-disable-next-line global-require
      const updatedApp = require('shared/app').App;
      render(updatedApp);
    });
  }
});
