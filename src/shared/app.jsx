import * as React from 'react';
import { createGlobalStyle } from 'styled-components';
import { COLORS } from 'shared/utils/style';
import { AppLayout } from 'shared/components/AppLayout';
import { RouteManager } from './route-manager';

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    color: ${COLORS.body};
    font-size: 16px;
    font-family: Roboto, Helvetica, Arial, sans-serif;
  }
`;

export const App = () => (
  <AppLayout>
    <GlobalStyle />
    <RouteManager />
  </AppLayout>
);
