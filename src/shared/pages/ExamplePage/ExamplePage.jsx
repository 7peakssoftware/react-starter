import * as React from 'react';

const ExamplePage = () => <span>This is an example of Async component loading/Code spliting</span>;

export default ExamplePage;
