import loadable from '@loadable/component';

export const AsyncExamplePage = loadable(() => import('./ExamplePage'));
