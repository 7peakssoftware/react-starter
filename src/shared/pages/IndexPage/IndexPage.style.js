import styled from 'styled-components';
import Banner from 'assets/images/banner.png';
import { Button } from 'shared/components/Button';

export const NavBar = styled.nav`
  display: flex;
  flex-shrink: 0;
  align-items: center;
  justify-content: space-between;
  height: 80px;
  margin: 1em;

  img {
    height: 50%;
  }
`;

export const Navigation = styled.ul`
  list-style-type: none;
`;

export const NavigationItem = styled.li`
  display: inline-block;
  box-sizing: border-box;
  padding: 10px;

  &.active {
    font-weight: bold;
  }

  a {
    color: inherit;
    text-decoration: none;
    text-transform: uppercase;
    border: solid 2px transparent;
  }
`;

export const Hero = styled.div`
  width: 100%;
  height: 300px;
  flex-shrink: 0;
  background-size: cover;
  background-position: center center;
  background-image: url(${Banner});
  transform: translate3d(0px, 0px, 0px);
`;

export const Content = styled.div`
  display: flex;
  flex-grow: 1;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin: 2em 1em;
  text-align: center;
`;

export const QuickStartButton = styled(Button)`
  margin: 1em;
  text-decoration: none;
`;
