import * as React from 'react';
import Logo from 'assets/images/logo.svg';
import { Routes } from 'shared/route-manager';
import { Link } from 'react-router-dom';
import {
  Hero,
  NavBar,
  Navigation,
  NavigationItem,
  Content,
  QuickStartButton
} from './IndexPage.style';

const { DOCS, SOURCE, EXAMPLE } = Routes;
const IndexPage = () => (
  <React.Fragment>
    <NavBar>
      <img id="logo" src={Logo} alt="7 Peaks Software Logo" />
      <Navigation>
        <NavigationItem>
          <a href={DOCS}>Docs</a>
        </NavigationItem>
        <NavigationItem>
          <a href={SOURCE}>Docs</a>
        </NavigationItem>
        <NavigationItem>
          <Link to={EXAMPLE}>Example Page</Link>
        </NavigationItem>
      </Navigation>
    </NavBar>
    <Hero />
    <Content>
      <h1>React Starter</h1>
      <p>A scaffolding project that contains a certain set of standard tools for a react app.</p>
      <QuickStartButton as="a" href={DOCS}>
        Quick Start
      </QuickStartButton>
    </Content>
  </React.Fragment>
);

export default IndexPage;
