import loadable from '@loadable/component';

export const AsyncIndexPage = loadable(() => import('./IndexPage'));
