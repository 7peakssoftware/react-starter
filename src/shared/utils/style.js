export const COLORS = Object.freeze({
  white: '#FFFFFF',
  body: '#8291a4',
  main: 'rgb(115, 192, 124)'
});

export const GUTTER = 8;
export const BORDER_RADIUS = 2;

export const spacing = (multiplier = 1) => `${GUTTER * multiplier}px`;
