import styled from 'styled-components';

export const AppLayout = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
`;
