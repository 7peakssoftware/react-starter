import styled from 'styled-components';
import { COLORS, spacing, BORDER_RADIUS } from 'shared/utils/style';

export const Button = styled.button`
  padding: ${spacing(2)};
  border: none;
  border-radius: ${BORDER_RADIUS}px;
  background-color: ${COLORS.main};
  color: ${COLORS.white};
  font-size: 1em;
  font-weight: bold;
  text-transform: uppercase;

  &:hover {
    cursor: pointer;
    background-color: rgb(115, 191, 124, 1.2);
  }
`;
