import * as React from 'react';
import { AsyncExamplePage } from 'shared/pages/ExamplePage';
import { Route, Switch } from 'react-router';
import { AsyncIndexPage } from 'shared/pages/IndexPage';

export const Routes = Object.freeze({
  INDEX: '/',
  EXAMPLE: '/example',
  DOCS: 'https://bitbucket.org/7peakssoftware/react-starter/src/master/README.md',
  SOURCE: 'https://bitbucket.org/7peakssoftware/react-starter/src/master/'
});

export const RouteManager = () => (
  <Switch>
    <Route path={Routes.EXAMPLE} component={AsyncExamplePage} />
    <Route exact to={Routes.INDEX} component={AsyncIndexPage} />
  </Switch>
);
