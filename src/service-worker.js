workbox.core.skipWaiting();
workbox.core.clientsClaim();

self.__precacheManifest = [].concat(self.__precacheManifest || []);

workbox.precaching.precache(self.__precacheManifest);

workbox.routing.registerRoute('/', ({ _, event }) => fetch(event.request).catch(() => caches.match(new Request('/cache-index.html'))));

workbox.precaching.addRoute({ directoryIndex: '/cache-index.html' });

workbox.routing.registerNavigationRoute('/cache-index.html');
