/* eslint-disable indent */
export const pageRenderer = ({
 initialState, children, styles, scripts
}) => `
  <!DOCTYPE html>
  <html lang="en">
    <head>
      <meta charSet="UTF-8" />
      <title>7 Peaks - React Starter</title>
      <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
      <meta name="apple-mobile-web-app-title" content="7 Peak Software - React Starter" />
      <meta name="apple-mobile-web-app-capable" content="yes" />
      <meta name="apple-mobile-web-app-status-bar-style" content="default" />
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
      <link rel="manifest" href="/manifest.json" />
      ${styles}
    </head>
    <body>
      <div id="entry">${children}</div>
      <script
        dangerouslySetInnerHTML={${{
          __html: `window.__INITIAL_STATE__ = ${JSON.stringify(initialState || {})};`
        }}}
      ></script>
      ${scripts}
    </body>
  </html>
`;
