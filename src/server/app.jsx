import React from 'react';
import { renderToString } from 'react-dom/server';
import { StaticRouter } from 'react-router';
import { pageRenderer } from 'server/views/page-renderer';
import { App } from 'shared/app';
import { ServerStyleSheet } from 'styled-components';
import { ChunkExtractor, ChunkExtractorManager } from '@loadable/server';

const initialState = {};
const context = {};

export default ({ clientStats }) => (req, res) => {
  const extractor = new ChunkExtractor({ stats: clientStats, entrypoints: 'client' });
  const sheet = new ServerStyleSheet();
  const body = renderToString(
    sheet.collectStyles(
      <StaticRouter location={req.url} context={context}>
        <ChunkExtractorManager extractor={extractor}>
          <App />
        </ChunkExtractorManager>
      </StaticRouter>
    )
  );
  const styles = sheet.getStyleTags();
  const scripts = extractor.getScriptTags();

  const html = pageRenderer({
    initialState,
    children: body,
    styles,
    scripts
  });

  return res.send(html);
};
