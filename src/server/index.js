/* eslint-disable no-console */
/* eslint-disable global-require */
/* eslint-disable import/no-unresolved */
const express = require('express');
const webpack = require('webpack');
const expressStaticGzip = require('express-static-gzip');

const isProd = process.env.NODE_ENV === 'production';
const isDev = !isProd;
const PORT = process.env.PORT || 8080;
let isBuilt = false;

const server = express();

const done = () => {
  if (isBuilt) {
    return;
  }

  server.listen(PORT, () => {
    isBuilt = true;
    console.log(`Server listening on ${PORT} in ${process.env.NODE_ENV}`);
  });
};

if (isDev) {
  const configClient = require('../../config/webpack-client.js');
  const configServer = require('../../config/webpack-server.js');
  const webpackHotServerMiddleware = require('webpack-hot-server-middleware');

  const bundler = webpack([configClient, configServer]);
  const [clientCompiler] = bundler.compilers;
  const webpackDevMiddleware = require('webpack-dev-middleware')(bundler, configClient.devServer);
  const webpackHotMiddleware = require('webpack-hot-middleware')(
    clientCompiler,
    configClient.devServer
  );

  server.use(webpackDevMiddleware);
  server.use(webpackHotMiddleware);
  server.use(webpackHotServerMiddleware(bundler));

  console.log('Middleware enabled');
  bundler.plugin('done', done);
} else {
  const clientStats = require('../../build/client-stats.json');
  const appMiddleware = require('../../build/bundle.server.js').default;

  server.use('/', expressStaticGzip('build', { enableBrotli: true }));
  server.use('*', appMiddleware({ clientStats }));

  done();
}
